Use `shared_ptr` + `weak_ptr` as follows:
```C++
struct Widget{
    struct Data{
      std::string message = "Hello!";      
      std::thread work;       
    }
    std::shared_ptr<Data> data = std::make_shared<Data>();
    
    Widget(){
      std::thread work{[data = std::weak_ptr<Data>(data)]() {
          while (true) {
              auto ptr = data.lock();
              if (!ptr) break;   

              std::cout << ptr->message << std::endl;
          }
      }};
      work.detach();         
    }
}
```


# DeferredUniquePtr
smart pointer, which owns object, but defer destruction if object is still in use (under lock).

Differs from `std::shared_ptr` - `shared_ptr` destroy object when there are no references to it (ptr counter == 0), `DeferredUniquePtr`/`DeferredSharedPtr` - when there are no locks (and destruction requested).

It is only safe to use `DeferredSharedPtr`'s store object under lock (`makeLock()`);

`DeferredUniquePtr`'s stored object is guaranteed to live at least till `DeferredUniquePtr` live, so there is no `makeLock()` for it.

# Example

```C++
struct Data {
    std::string text;
};
{
    DeferredUniquePtr<Data> data(new Data{"Hello!"});
    std::thread t{[data = data.ref()]() {          // ref() returns DeferredSharedPtr
        while (true) {
            auto lock = data.makeLock();
            if (!lock) break;                      // data is already destroyed?
            // data will not be destroyed while under lock

            std::this_thread::sleep_for(std::chrono::milliseconds(1700));  // doing some hard work

            std::cout << data->text << std::endl;
        }
    }};
    t.detach();

    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "END" << std::endl;
}
```

Output:
```
END
Hello!
```


# Motivation

Widget do some background work in thread, and update message with result. 

As soon, as widget "destroyed", background work should stop.

01 (wrong):
```C++
struct Widget{
    std::atomic<bool> alive = true;
    std::string message = "Hello!";
    std::thread work{[&]() {
        while (alive) {
            // message may already not exist at this moment
            std::cout << message << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }};
    work.detach();

    ~Widget(){
      alive = false;
    }
}
```

02 (add mutex):
```C++
struct Widget{
    std::mutex lock;
    bool alive = true;
    std::string message = "Hello!";
    std::thread work{[&]() {
        while (true) {
            std::unique_lock<std::mutex> l(lock);
            if (!alive) return;
            
            std::cout << message << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));   // do some work here....
        }
    }};
    work.detach();
    
    ~Widget(){
      // may block for 1 second !!!
      // critical if happens on UI thread      
      std::unique_lock<std::mutex> l(lock);
      alive = false;
    }
}
```

03(solution):
```C++
struct Widget{
    struct Data{
      std::string message = "Hello!";      
      std::thread work;       
    }
    DeffredUniquePtr<Data> data{new Data()};
    
    Widget(){
      std::thread work{[data = data.ref()]() {
          while (true) {
              auto lock = data.makeLock();
              if (!lock) break;   // already destroyed?
              // will not be destroyed while under lock

              std::cout << message << std::endl;
              std::this_thread::sleep_for(std::chrono::seconds(1));   // do some work here....
          }
      }};
      work.detach();         
    }
}
```


04(shared_ptr)
```C++
struct Widget{
    struct Data{
      std::string message = "Hello!";
      std::atomic<bool> alive{true};
      
      std::thread work1;
    }
    std::shared_ptr<Data> data = std::make_shared<Data>();
    
    Widget(){
      data->work1 = [optional<std::shared_ptr<Data>> = data /* to avoid cyclic refernce, may not be necessary here*/]() mutable{
          while (true) {
              if (!data->alive){
                 data = {};
                 return;
              }

              std::cout << message << std::endl;
              std::this_thread::sleep_for(std::chrono::seconds(1));   // do some work here....
          }
      };
      data->work1.detach();              
    }
    
    ~Widget(){
       data->alive = false;
    }
}
```


----

Message, updates periodically from thread 1.
At some point from thread2 Message may be destroyed.

1(wrong):
```C++
    std::atomic<std::string*> message = new std::string("Hello!");
    std::thread t{[&]() {
        while (message) {
            // message may be corrupted here
            std::cout << *message << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }};
    t.detach();
    
    std::thread t2{[&]() {
      ...
      if (smthg_happened){
         delete message;
         message = nullptr;
      }
    }};
    t.detach();
```


2(mutex):
```C++
    std::mutex lock;
    std::atomic<std::string*> message = new std::string("Hello!");
    std::thread t{[&]() {
        while (message) {
            std::unique_lock<std::mutex> l(lock);
            
            std::cout << *message << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }};
    t.detach();
    
    std::thread t2{[&]() {
      ...
      if (smthg_happened && message){
         // may block here for 1 second
         std::unique_lock<std::mutex> l(lock);
         delete message;
         message = nullptr;
      }
    }};
    t.detach();
```


3 (shared_ptr):
```C++    
    struct Data{
      std::string message = "Hello!";
      std::atomic<bool> alive{true};
    };

    std::shared_ptrData> message = new Data{"Hello!"};
    std::thread t{[optional<std::shared_ptr<Data> msg = message]() {
        while (true) {
              if (!msg->alive){
                 msg = {};
                 return;
              }
            
            std::cout << *message << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }};
    t.detach();
    
    std::thread t2{[optional<std::shared_ptr<Data>>> msg = message]() {
      ...
      if (smthg_happened && message){
         msg->alive = false;
         msg = {};
      }
    }};
    t.detach();
```


4 (DeferredSharedPtr):
```C++    
    DefferedSharedPtr<std::string> message{ new std::string("Hello!") };
    std::thread t{[message]() {
        while (true) {
            { /* object liveness lock localized here */
                auto lock = message.makeLock();
                if (!lock) break;
                
                std::cout << *message << std::endl;
            }
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }};
    t.detach();
    
    std::thread t2{[message]() {
      ...
      if (smthg_happened){
        message.schedule_delete();  // as soon as there will be no lock holded string will be deleted.
      }
    }};
    t.detach();
```


5 (weak_ptr):
```C++    
    std::shared_ptr<std::string> message{ new std::string("Hello!") };
    std::thread t{[message = std::weak_ptr<std::string>(message)]() {
        while (true) {
            { /* object liveness lock localized here */
                auto ptr = message.lock();
                if (!ptr) break;
                
                std::cout << *ptr << std::endl;
            }
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }};
    t.detach();
    
    std::thread t2{[&, message = std::weak_ptr<std::string>(message)]() {
      ...
      if (smthg_happened){
        this->message.reset();  // as soon as there will be no lock holded string will be deleted.
      }
    }};
    t2.detach();
```

# Conclusion

 * Though it is possible to achieve the same with `std::shared_ptr + std::optional<std::shared_ptr> + std::atomic<bool>`, DeferredSharedPtr/DeferredUniquePtr will release object MUCH earlier - as soon as there are nobody in the lock sections.
 * DeferredUniquePtr does not have cyclic reference problem.
 * [DeferredSharedPtr still may suffer from cyclic referencing (such as shared_ptr), but this issue is solvable with schedule_delete call]
 * Plus - more readable syntax.
