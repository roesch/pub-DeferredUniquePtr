#pragma once

#include <memory>
#include <atomic>
#include <experimental/optional>



template<class>
class DeferredUniquePtr;


// do avoid cyclic refernces with DefferedUniquePtr
//  may be schedule_destroy
//      Object destructs as only there are no locks and destruction requested
template<class T>
class DeferredSharedPtr{
    friend DeferredUniquePtr<T>;

    template<class OT>
    using optional = std::experimental::optional<OT>;

    struct Data{
        T* value;

        std::atomic_flag destructed =  ATOMIC_FLAG_INIT /*false*/;
        std::atomic<bool> alive{true};
        std::atomic<int> lock_count{0};

        Data(const Data&) = delete;
        Data(Data&&) = delete;

        Data(T* value)
            : value( value )
        {}

        void check_and_destroy(){
            int i = lock_count;
            const bool destroy = (alive == false && lock_count == 0);
            if (!destroy) return;

            if (destructed.test_and_set()) return;
            delete value;
        }

        ~Data(){
            if (destructed.test_and_set()) return;
            delete value;
        }
    };
    std::shared_ptr<Data> data;

public:
    DeferredSharedPtr(T* value)
        : data{std::make_shared<Data>(value)}
    {}

    class Lock{
        Data* data;
    public:
        Lock(const Lock&) = delete;
        Lock(Lock&& other)
            :data(other.data){
            other.data = nullptr;
        }

        Lock(Data* data)
            :data(data)
        {
            data->lock_count++;
        }

        ~Lock(){
            if (!data) return;
            data->lock_count--;
            data->check_and_destroy();
        }
    };

    optional<Lock> makeLock() const {
        Lock lock{data.get()};
        if (data->alive){
            return {std::move(lock)};
        } else {
            return {};
        }
    }

    bool alive() const{
        return data->alive;
    }

    T* get() const{
        return data->value;
    }
    T* operator->() const{
        return get();
    }

    void schedule_destroy(){
        data->alive = false;
        data->check_and_destroy();
    }
};


//  Object destruction deferred, till there are no locks in references
template<class T>
class DeferredUniquePtr{
    DeferredSharedPtr<T> data;
public:
    DeferredUniquePtr(const DeferredUniquePtr&) = delete;

    using Ref = DeferredSharedPtr<T>;
    Ref ref() {
        return data;
    }

    // forward accessors
    T* get() const{
        return data->get();
    }
    T* operator->() const{
        return get();
    }

    DeferredUniquePtr(T* value)
        : data(value)
    {
        data.data->lock_count++;   // prevent destruction for a DeferredUniquePtr lifetime
    }

    // TODO: void reset(T*)
    // TODO: void release()

    ~DeferredUniquePtr(){
        data.data->lock_count--;
        data.schedule_destroy();
    }
};