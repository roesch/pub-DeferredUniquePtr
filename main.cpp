#include <iostream>
#include <string>
#include <atomic>
#include <thread>
#include <functional>
#include <vector>
#include <memory>
#include <deque>
#include <chrono>

#include "DeferredUniquePtr.h"


void test_DeferredUniquePtr(){
    struct Data {
        std::string text;
    };

    {
        DeferredUniquePtr<Data> data2(new Data{"Hello!"});
        std::thread t{[data = data2.ref()]() {
            while (true) {
                auto lock = data.makeLock();
                if (!lock) break;

                std::this_thread::sleep_for(std::chrono::milliseconds(1700));

                std::cout << data->text << std::endl;
            }
        }};
        t.detach();

        std::this_thread::sleep_for(std::chrono::seconds(1));
        std::cout << "END" << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::seconds(3));
}

int main() {
    test_DeferredUniquePtr();
    return 0;
}
